# b2bic_php_sdk

#### 介绍
平安银行b2bic客户端php版sdk

#### 更新说明
+ 2020年9月08日 017 修改了台账查询接口从对象数组改为数组
+ 2020年9月08日 016 修改了已知问题(对接接口大小写不正确)
+ 2020年9月07日 015 修改了已知问题
+ 2020年8月31日 014 增加台账查询功能，增加了查询次数功能
+ 2020年8月27日 013 修复了已知问题(返回类改名后不正确问题)
+ 2020年8月26日 012 修复了已知问题
+ 2020年8月26日 011 增加了支付订单状态查询
+ 2020年8月18日 010 修复了支付请求时漏掉了主账户名的问题
+ 2020年8月04日 009 修改了获取子账户顺序码的函数错误，修改了获取子账户顺序码验证器错误
+ 2020年7月29日 008 增加了查询子账户的列表 AccountList
+ 2020年7月29日 007 增加了子账户余额查询模块 AccountMoney
+ 2020年7月29日 006 增加了子账户支付模块 AccountPay
+ 2020年7月29日 005 修改了底层返回逻辑，直接返回格式化的应答对象
+ 2020年7月28日 004 增加了设置URL
+ 2020年7月28日 003 修复了已知问题，增加了debug记录日志功能
+ 2020年7月28日 002 增加了子账户序号末位验证位生成与验证方法
+ 2020年7月28日 001 已完成子账户维护模块


#### 安装教程

1.  composer require "jancojie/b2bic_php_sdk"

#### 使用说明


```php
    use B2bic\Enum\AccountOpFlag;
    use B2bic\Request\Account;
    use B2bic\Request\B2bicRequest;

    $account = new Account();
    $account->setMainAccount('15000100716415');
    $account->setSubAccountSeq('250015');
    $account->setSubAccountName('广州悦盈医疗投资管理有限公司');
    $account->setOpFlag(AccountOpFlag::新建);

    $req = new B2bicRequest();
    $req->setUrl('127.0.0.1:7072')
        ->setCustomerId('00901080000008888000')
        ->setCode(date("YmdHis") . rand(1111, 9999));
    try {
        $respons = $req->execute($account,true);
        if($respons->responseCode == '000000'){
            $obj = $respons->bodyObject;
            var_dump($obj);
        }else{
            echo "接口调用失败：{$respons->resonseMsg}";
        }
        
    } catch (Exception $e) {
        echo $e->getMessage();
    }
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

