<?php

namespace B2bic\Response;

use B2bic\Commond\B2bicResponsetInterface;
use B2bic\Enums\Account\AccountInterestFlag;
use B2bic\Enums\Account\AccountSettleInterestCycle;

/**
 * 子账户列表
 *  @param $MainAccount 主账号
 *  @param $CcyCode 币种
 *  @param $AccountName 子账户户名
 *  @param $Stt 状态
 *  @param $LastModifyDate 最后维护日期
 *  @param $CorId 公司码
 *  @param $SubAccNum 子帐号数量
 *  @param $IsEnd 是否结束标志
 *  @param $Count 出记录条数
 *  @param $list 子账户列表
 */
class ResponseAccountList implements B2bicResponsetInterface
{

    private $MainAccount = ''; //主账号
    private $CcyCode = ''; //币种
    private $AccountName = ''; //子账户户名
    private $Stt = ''; //状态
    private $LastModifyDate = ''; //最后维护日期
    private $CorId = ''; //公司码
    private $SubAccNum = ''; //子帐号数量
    private $IsEnd = ''; //是否结束标志
    private $Count = ''; //输出记录条数
    private $list = []; //子账户列表


    public function getMainAccount()
    {
        return $this->MainAccount;
    }

    public function getCcyCode()
    {
        return $this->CcyCode;
    }

    public function getAccountName()
    {
        return $this->AccountName;
    }

    public function getStt()
    {
        return $this->Stt;
    }

    public function getLastModifyDate()
    {
        return $this->LastModifyDate;
    }

    public function getCorId()
    {
        return $this->CorId;
    }

    public function getSubAccNum()
    {
        return $this->SubAccNum;
    }

    public function getIsEnd()
    {
        return $this->IsEnd;
    }

    public function getCount()
    {
        return $this->Count;
    }

    public function getlist()
    {
        return $this->list;
    }

    public static function setParam($data)
    {
        $obj = new self();
        if (is_array($data)) {
            $data = array_filter($data);
            foreach ($data as $key => $value) {
                $obj->$key = $value;
            }
        }
        return $obj;
    }
}
