<?php

namespace B2bic\Response;

use B2bic\Commond\B2bicResponsetInterface;


class ResponseAccountPay implements B2bicResponsetInterface
{

    private $ThirdVoucher = ''; //支付流水号
    private $FrontFlowNo = ''; //银行流水号
    private $CstInnerFlowNo = ''; //企业自定义凭证号
    private $MainAccount = ''; //主账号
    private $OutSubAccount = ''; //付款子账号
    private $OutSubAccBalance = ''; //付款子账号余额
    private $CcyCode = ''; //币种
    private $TranAmount = ''; //付款金额
    private $InSubAccNo = ''; //收款子账号
    private $InSubAccName = ''; //收款子账号
    private $InSubAccBalance = ''; //收款子账号余额

    public function getMainAccount()
    {
        return $this->MainAccount;
    }

    public function getThirdVoucher()
    {
        return $this->ThirdVoucher;
    }

    public function getFrontFlowNo()
    {
        return $this->FrontFlowNo;
    }

    public function getCstInnerFlowNo()
    {
        return $this->CstInnerFlowNo;
    }

    public function getOutSubAccount()
    {
        return $this->OutSubAccount;
    }

    public function getOutSubAccBalance()
    {
        return $this->OutSubAccBalance;
    }

    public function getCcyCode()
    {
        return $this->CcyCode;
    }

    public function getTranAmount()
    {
        return $this->TranAmount;
    }

    public function getInSubAccNo()
    {
        return $this->InSubAccNo;
    }
    public function getInSubAccName()
    {
        return $this->InSubAccName;
    }
    public function getInSubAccBalance()
    {
        return $this->InSubAccBalance;
    }

    public static function setParam($data)
    {
        $obj = new self();
        if (is_array($data)) {
            $data = array_filter($data);
            foreach ($data as $key => $value) {
                $obj->$key = $value;
            }
        }
        return $obj;
    }
}
