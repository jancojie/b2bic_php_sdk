<?php

namespace B2bic\Response;

use B2bic\Model\Ledger;
use B2bic\Commond\B2bicResponsetInterface;


class ResponseLedgerSearch implements B2bicResponsetInterface
{
    private $Count = ''; //当前页输出记录条数
    private $AllCount = ''; //满足输入条件的所有记录数
    private $IsEnd = ''; //结束标志
    private $list  = ''; //数据列表

    public function getCount()
    {
        return $this->Count;
    }

    public function getAllCount()
    {
        return $this->AllCount;
    }

    public function getIsEnd()
    {
        return $this->IsEnd;
    }

    public function getList()
    {
        return $this->list;
    }

    public static function setParam($data)
    {
        $obj = new self();
        if (is_array($data)) {
            $data = array_filter($data);
            foreach ($data as $key => $value) {
                // if($key == 'list' && is_array($value)){
                //     $obj->$key = array();
                //     foreach ($value as $row){
                //         $ledger = new Ledger($row);
                //         array_push($obj->$key,$ledger);
                //     }
                // }else{
                    // if(is_array($value)&&count($value) == 0){
                    //     $value = null;
                    // }
                    $obj->$key = $value;
                // }
                
            }
        }
        return $obj;
    }
}
