<?php

namespace B2bic\Response;

use B2bic\Commond\B2bicResponsetInterface;


class ResponsePayOrderSearch implements B2bicResponsetInterface
{
    private $CstInnerFlowNo = ''; //客户自定义凭证号
    private $CcyCode = ''; //货币类型
    private $OutAcctNo = ''; //转出账户
    private $InAcctBankName = ''; //转入账户网点名称
    private $InAcctNo = ''; //转入账户
    private $InAcctName = ''; //转入账户户名
    private $TranAmount = ''; //交易金额
    private $UnionFlag = ''; //行内跨行标志 1：行内转账，0：跨行转账
    private $Stt = ''; //交易状态标志20：成功 30：失败 其他为银行受理成功处理中
    private $IsBack = ''; //转账退票标志
    private $BackRem = ''; //支付失败或退票原因描述
    private $Yhcljg = ''; //银行处理结果 000000：转账成功
    private $Fee = ''; //转账手续费
    private $submitTime = ''; //交易受理时间
    private $AccountDate = ''; //记账日期
    private $hostFlowNo = ''; //主机记账流水号
    private $SysFlag = ''; //转账加急标志 Y：加急 N：普通S：特急

    public function getCstInnerFlowNo()
    {
        return $this->CstInnerFlowNo;
    }

    public function getCcyCode()
    {
        return $this->CcyCode;
    }

    public function getOutAcctNo()
    {
        return $this->OutAcctNo;
    }

    public function getInAcctBankName()
    {
        return $this->InAcctBankName;
    }

    public function getInAcctNo()
    {
        return $this->InAcctNo;
    }

    public function getInAcctName()
    {
        return $this->InAcctName;
    }

    public function getTranAmount()
    {
        return $this->TranAmount;
    }

    public function getUnionFlag()
    {
        return $this->UnionFlag;
    }

    public function getStt()
    {
        return $this->Stt;
    }
    public function getIsBack()
    {
        return $this->IsBack;
    }
    public function getBackRem()
    {
        return $this->BackRem;
    }
    public function getYhcljg()
    {
        return $this->Yhcljg;
    }

    public function getSysFlag()
    {
        return $this->SysFlag;
    }

    public function getFee()
    {
        return $this->Fee;
    }

    public function getsubmitTime()
    {
        return $this->submitTime;
    }

    public function getAccountDate()
    {
        return $this->AccountDate;
    }

    public function gethostFlowNo()
    {
        return $this->hostFlowNo;
    }

    public static function setParam($data)
    {
        $obj = new self();
        if (is_array($data)) {
            $data = array_filter($data);
            foreach ($data as $key => $value) {
                $obj->$key = $value;
            }
        }
        return $obj;
    }
}
