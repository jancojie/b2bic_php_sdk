<?php

namespace B2bic\Response;

use B2bic\Commond\B2bicResponsetInterface;


class ResponseAccountMoney implements B2bicResponsetInterface
{

    private $SubAccountNo = ''; //子帐号
    private $MainAccount = ''; //主账号
    private $CcyCode = ''; //币种
    private $SubAccountName = ''; //子账户户名
    private $SubAccBalance = 0; //子账户余额

    public function getMainAccount()
    {
        return $this->MainAccount;
    }

    public function getSubAccountNo()
    {
        return $this->SubAccountNo;
    }

    public function getSubAccountName()
    {
        return $this->SubAccountName;
    }

    public function getCcyCode()
    {
        return $this->CcyCode;
    }

    public function getSubAccBalance()
    {
        return $this->SubAccBalance;
    }

    public static function setParam($data)
    {
        $obj = new self();
        if (is_array($data)) {
            $data = array_filter($data);
            foreach ($data as $key => $value) {
                $obj->$key = $value;
            }
        }
        return $obj;
    }
}
