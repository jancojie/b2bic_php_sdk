<?php

namespace B2bic\Response;

use B2bic\Commond\B2bicRequestInterface;
use B2bic\Commond\Tool;

class B2bicResponse
{
    public $version = '';
    public $sysId = '';
    public $charset = ''; //01：GBK 02：UTF8 03：unicode 04：iso-8859-1
    public $methon = ''; //01:tciip 02:http 03webservice
    public $customerId = '';
    public $bodyLength = 0;
    public $payCode = '';
    public $operationId = '';
    public $type = '';
    public $payDay = '';
    public $payTime = '';
    public $code = '';
    public $responseCode = '';
    public $resonseMsg = '';
    public $isEnd = '';
    public $requestTimes = '';
    public $signType = '';
    public $signDataType = '';
    public $signMethen = '';
    public $signLenth = 0;
    public $fileSize = 0;
    public $body = '';
    public $requestStr = '';
    public $bodyObject;
    public $bodyArr = [];

    public function __construct($str = '', B2bicRequestInterface $res = null)
    {
        if ($str != '') {
            $this->reduceReturn($str, $res);
        }
    }

    public function reduceReturn($str, B2bicRequestInterface $res)
    {

        $this->requestStr = $str;
        $this->version = substr($str, 0, 4);
        $this->sysId = substr($str, 4, 2);
        $this->charset = substr($str, 6, 2);
        $this->methon = substr($str, 8, 2);
        $this->customerId = substr($str, 10, 20);
        $this->bodyLength = substr($str, 30, 10);
        $this->payCode = substr($str, 40, 6);
        $this->operationId = substr($str, 46, 5);
        $this->type = substr($str, 51, 2);
        $this->payDay = substr($str, 53, 8);
        $this->payTime = substr($str, 61, 6);
        $this->code = substr($str, 67, 20);
        $this->responseCode = substr($str, 87, 6);
        $this->resonseMsg = substr($str, 93, 100);
        $this->isEnd = substr($str, 193, 1);
        $this->requestTimes = substr($str, 194, 3);
        $this->signType = substr($str, 197, 1);
        $this->signDataType = substr($str, 198, 1);
        $this->signMethen = substr($str, 199, 12);
        $this->signLenth = substr($str, 211, 10);
        $this->fileSize = substr($str, 221, 1);
        $this->body = substr($str, 222, intval($this->bodyLength));
        $this->bodyArr = Tool::xmlToArr($this->body);
        $this->bodyObject = $res->getRespons($this->bodyArr);
    }
}
