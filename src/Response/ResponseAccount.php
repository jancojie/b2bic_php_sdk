<?php

namespace B2bic\Response;

use B2bic\Commond\B2bicResponsetInterface;
use B2bic\Enums\Account\AccountInterestFlag;
use B2bic\Enums\Account\AccountSettleInterestCycle;


/**
 * 子账户查询结果对象
 * @param String $MainAccount
 * @param String $SubAccountNo
 * @param String $SubAccountName
 * @param String $Stt
 * @param String $LastModifyDate
 * @param String $InterestFlag
 * @param String $SettleInterestCycle
 * @param String $Rate
 */
class ResponseAccount implements B2bicResponsetInterface
{

    private $MainAccount = ''; //主账号
    private $SubAccountNo = ''; //子账户
    private $SubAccountName = ''; //子账户户名
    private $Stt = ''; //子账号状态
    private $LastModifyDate = ''; //最后维护日期
    private $InterestFlag = ''; //内部计息标志
    private $SettleInterestCycle = ''; //内部计息周期
    private $Rate = ''; //计息利率


    public function getMainAccount()
    {
        return $this->MainAccount;
    }

    public function getSubAccountNo()
    {
        return $this->SubAccountNo;
    }

    public function getSubAccountName()
    {
        return $this->SubAccountName;
    }

    public function getStt()
    {
        return $this->Stt;
    }

    public function getLastModifyDate()
    {
        return $this->LastModifyDate;
    }

    public function getSettleInterestCycle()
    {
        return AccountSettleInterestCycle::getkey($this->SettleInterestCycle);
    }

    public function getInterestFlag()
    {
        return  AccountInterestFlag::getKey($this->InterestFlag);
    }

    public function getRate()
    {
        return $this->Rate;
    }


    public static function setParam($data)
    {
        $obj = new self();
        if (is_array($data)) {
            $data = array_filter($data);
            foreach ($data as $key => $value) {
                $obj->$key = $value;
            }
        }
        return $obj;
    }
}
