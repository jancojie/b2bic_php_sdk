<?php

namespace B2bic\Enums\Account;

use BenSampo\Enum\Enum;

final class AccountODFlag  extends Enum{
    const 非透支 = 'N';
    const 透支 = 'Y';
}