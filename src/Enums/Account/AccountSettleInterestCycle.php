<?php

namespace B2bic\Enums\Account;

use BenSampo\Enum\Enum;

/**
 * 内部计息标志
 */
final class AccountSettleInterestCycle  extends Enum{
    const 按日 = 'D';
    const 按月 = 'M';
    const 按季 = 'Q';
    const 按年 = 'Y';
}