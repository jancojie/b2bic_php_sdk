<?php

namespace B2bic\Enums\Account;

use BenSampo\Enum\Enum;

final class LedgerSearchOpFlag  extends Enum{
    const 按智能收款主帐号 = '1';
    const 按子账户查询 = '2';
}