<?php

namespace B2bic\Enums\Account;

use BenSampo\Enum\Enum;

final class AccountOpFlag  extends Enum{
    const 新建 = 'A';
    const 修改 = 'U';
    const 删除 = 'D';
    const 恢复 = 'R';
    const 同步权限 = 'B';
}