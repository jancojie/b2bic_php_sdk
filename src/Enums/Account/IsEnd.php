<?php

namespace B2bic\Enums\Account;

use BenSampo\Enum\Enum;

/**
 * 内部计息标志
 */
final class IsEnd  extends Enum{
    const 是 = 'Y';
    const 否 = 'N';
}