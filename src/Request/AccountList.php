<?php

namespace B2bic\Request;

use B2bic\Enums\Account\AccountODFlag;
use B2bic\Commond\B2bicRequestInterface;
use B2bic\Enums\Account\AccountInterestFlag;
use B2bic\Response\ResponseAccountList;

/**
 * 子账户维护
 * @param String $MainAccount
 * @param String $CcyCode
 * @param String $ReqSubAccountNo
 * @param String $PageNo
 */
class AccountList implements B2bicRequestInterface
{


    private $payCode = 'C001';
    private $MainAccount = ''; //主账号
    private $CcyCode = ''; //币种
    private $ReqSubAccountNo = ''; //子账户
    private $PageNo = ''; //页码


    public function setMainAccount($MainAccount)
    {
        $this->MainAccount = $MainAccount;
    }

    public function setCcyCode($CcyCode)
    {
        $this->CcyCode = $CcyCode;
    }

    public function setReqSubAccountNo($ReqSubAccountNo)
    {
        $this->ReqSubAccountNo = $ReqSubAccountNo;
    }

    public function setPageNo($PageNo)
    {
        $this->PageNo = $PageNo;
    }

    public function getParam()
    {
        return array_filter(get_object_vars($this));
    }

    public function getRespons($data)
    {
        return ResponseAccountList::setParam($data);
    }

    public function getPayCode()
    {
        return $this->payCode;
    }
}
