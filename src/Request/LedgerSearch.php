<?php

namespace B2bic\Request;

use B2bic\Commond\B2bicRequestInterface;
use B2bic\Response\ResponseLedgerSearch;



/**
 * 台账查询
 * @param String $MainAccount
 * @param String $ReqSubAccountNo
 * @param String $CcyCode
 */
class LedgerSearch implements B2bicRequestInterface
{


    private $payCode = 'C00602';
    private $MainAccount = ''; //主账户
    private $OpFlag = ''; //查询方式
    private $ReqSubAccount = ''; //子账号
    private $StartTime = ''; //开始日期
    private $EndTime = ''; //结束日期
    private $Remark = '0'; //备注
    private $DCFlag = '0'; //借贷标志
    private $PageNo = '0'; //页码
    private $PageSize = ''; //每页笔数

    public function setMainAccount($MainAccount)
    {
        $this->MainAccount = $MainAccount;
    }

    public function setOpFlag($OpFlag)
    {
        $this->OpFlag = $OpFlag;
    }

    public function setReqSubAccount($ReqSubAccount)
    {
        $this->ReqSubAccount = $ReqSubAccount;
    }


    public function setEndTime($EndTime)
    {
        $this->EndTime = $EndTime;
    }


    public function setStartTime($StartTime)
    {
        $this->StartTime = $StartTime;
    }


    public function setRemark($Remark)
    {
        $this->Remark = $Remark;
    }


    public function setDCFlag($DCFlag)
    {
        $this->DCFlag = $DCFlag;
    }


    public function setPageNo($PageNo)
    {
        $this->PageNo = $PageNo;
    }


    public function setPageSize($PageSize)
    {
        $this->PageSize = $PageSize;
    }

    public function getParam()
    {
        return array_filter(get_object_vars($this));
    }

    public function getRespons($data)
    {
        return ResponseLedgerSearch::setParam($data);
    }

    public function getPayCode()
    {
        return $this->payCode;
    }
}
