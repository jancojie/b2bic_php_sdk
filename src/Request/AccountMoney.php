<?php

namespace B2bic\Request;

use B2bic\Enums\Account\AccountODFlag;
use B2bic\Commond\B2bicRequestInterface;
use B2bic\Enums\Account\AccountInterestFlag;
use B2bic\Response\ResponseAccountMoney;

/**
 * 子账户余额查询
 * @param String $MainAccount
 * @param String $ReqSubAccountNo
 * @param String $CcyCode
 */
class AccountMoney implements B2bicRequestInterface
{


    private $payCode = 'C008';
    private $MainAccount = ''; //主账号
    private $ReqSubAccountNo = ''; //子帐号
    private $CcyCode = ''; //币种

    public function setMainAccount($MainAccount)
    {
        $this->MainAccount = $MainAccount;
    }

    public function setReqSubAccountNo($ReqSubAccountNo)
    {
        $this->ReqSubAccountNo = $ReqSubAccountNo;
    }

    public function setCcyCode($CcyCode)
    {
        $this->CcyCode = $CcyCode;
    }

    public function getParam()
    {
        return array_filter(get_object_vars($this));
    }

    public function getRespons($data)
    {
        return ResponseAccountMoney::setParam($data);
    }

    public function getPayCode()
    {
        return $this->payCode;
    }
}
