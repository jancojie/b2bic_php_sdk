<?php

namespace B2bic\Request;

use B2bic\Enums\Account\AccountODFlag;
use B2bic\Response\ResponseAccountPay;
use B2bic\Commond\B2bicRequestInterface;
use B2bic\Enums\Account\AccountInterestFlag;


/**
 * 子账户转账
 * @param String $ThirdVoucher
 * @param String $CstInnerFlowNo
 * @param String $MainAccount
 * @param String $MainAccountName
 * @param String $CcyCode
 * @param String $OutSubAccount
 * @param String $OutSubAccountName
 * @param double $TranAmount
 * @param String $InSubAccNo
 * @param String $InSubAccName
 * @param String $UseEx
 */
class AccountPay implements B2bicRequestInterface
{

    private $payCode = 'C005';
    private $ThirdVoucher = ''; //支付流水号
    private $CstInnerFlowNo = ''; //企业自定义凭证号/订单号
    private $MainAccount = ''; //主账号
    private $MainAccountName = ''; //主账号户名
    private $CcyCode = ''; //币种
    private $OutSubAccount = ''; //付款子账号
    private $OutSubAccountName = ''; //子账号户名
    private $TranAmount = 0; //付款金额
    private $InSubAccNo = ''; //收款子账号
    private $InSubAccName = ''; //收款子账号户名
    private $UseEx = ''; //转账附言

    public function setThirdVoucher($ThirdVoucher)
    {
        $this->ThirdVoucher = $ThirdVoucher;
    }

    public function setCstInnerFlowNo($CstInnerFlowNo)
    {
        $this->CstInnerFlowNo = $CstInnerFlowNo;
    }

    public function setMainAccount($MainAccount)
    {
        $this->MainAccount = $MainAccount;
    }

    public function setMainAccountName($MainAccountName)
    {
        $this->MainAccountName = $MainAccountName;
    }
    public function setCcyCode($CcyCode)
    {
        $this->CcyCode = $CcyCode;
    }

    public function setOutSubAccount($OutSubAccount)
    {
        $this->OutSubAccount = $OutSubAccount;
    }

    public function setOutSubAccountName($OutSubAccountName)
    {
        $this->OutSubAccountName = $OutSubAccountName;
    }

    public function setTranAmount($TranAmount)
    {
        $this->TranAmount = $TranAmount;
    }

    public function setInSubAccNo($InSubAccNo)
    {
        $this->InSubAccNo = $InSubAccNo;
    }

    public function setInSubAccName($InSubAccName)
    {
        $this->InSubAccName = $InSubAccName;
    }

    public function setUseEx($UseEx)
    {
        $this->UseEx = $UseEx;
    }

    public function getParam()
    {
        return array_filter(get_object_vars($this));
    }

    public function getRespons($data)
    {
        return ResponseAccountPay::setParam($data);
    }

    public function getPayCode()
    {
        return $this->payCode;
    }
}
