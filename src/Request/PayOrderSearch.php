<?php

namespace B2bic\Request;

use B2bic\Commond\B2bicRequestInterface;
use B2bic\Response\ResponsePayOrderSearch;



/**
 * 支付单状态查询
 * @param String $MainAccount
 * @param String $ReqSubAccountNo
 * @param String $CcyCode
 */
class PayOrderSearch implements B2bicRequestInterface
{


    private $payCode = 'C007';
    private $OrigThirdVoucher = ''; //转账凭证号
    private $OrigFrontLogNo = ''; //银行流水号

    public function setOrigThirdVoucher($OrigThirdVoucher)
    {
        $this->OrigThirdVoucher = $OrigThirdVoucher;
    }

    public function setOrigFrontLogNo($OrigFrontLogNo)
    {
        $this->OrigFrontLogNo = $OrigFrontLogNo;
    }

    public function getParam()
    {
        return array_filter(get_object_vars($this));
    }

    public function getRespons($data)
    {
        return ResponsePayOrderSearch::setParam($data);
    }

    public function getPayCode()
    {
        return $this->payCode;
    }
}
