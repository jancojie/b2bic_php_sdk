<?php

namespace B2bic\Request;

use B2bic\Response\ResponseAccount;
use B2bic\Enums\Account\AccountODFlag;
use B2bic\Commond\B2bicRequestInterface;
use B2bic\Enums\Account\AccountInterestFlag;


/**
 * 子账户维护
 * @param String $MainAccount
 * @param String $CcyCode
 * @param String $SubAccountSeq
 * @param String $SubAccount
 * @param String $SubAccountName
 * @param String $OpFlag
 * @param String $ODFlag
 * @param String $InterestFlag
 * @param String $SettleInterestCycle
 * @param int $Rate
 */
class Account implements B2bicRequestInterface
{


    private $payCode = 'C002';
    private $MainAccount = ''; //主账号
    private $CcyCode = ''; //币种
    private $SubAccountSeq = ''; //子账户序号
    private $SubAccount = ''; //子账号
    private $SubAccountName = ''; //子账户户名
    private $OpFlag = ''; //功能码
    private $ODFlag = AccountODFlag::非透支; //子账户透支标志
    private $InterestFlag = AccountInterestFlag::否; //内部计息标志
    private $SettleInterestCycle; //内部计息周期
    private $Rate; //计息利率

    public function setMainAccount($MainAccount)
    {
        $this->MainAccount = $MainAccount;
    }

    public function setCcyCode($CcyCode)
    {
        $this->CcyCode = $CcyCode;
    }

    public function setSubAccountSeq($SubAccountSeq)
    {
        $this->SubAccountSeq = $SubAccountSeq;
    }

    public function setSubAccount($SubAccount)
    {
        $this->SubAccount = $SubAccount;
    }

    public function setSubAccountName($SubAccountName)
    {
        $this->SubAccountName = $SubAccountName;
    }

    public function setOpFlag($OpFlag)
    {
        $this->OpFlag = $OpFlag;
    }

    public function setODFlag($ODFlag)
    {
        $this->ODFlag = $ODFlag;
    }

    public function setInterestFlag($InterestFlag)
    {
        $this->InterestFlag = $InterestFlag;
    }

    public function SetSettleInterestCycle($SettleInterestCycle)
    {
        $this->SettleInterestCycle = $SettleInterestCycle;
    }

    public function SetRate($Rate)
    {
        $this->Rate = $Rate;
    }

    public function getParam()
    {
        return array_filter(get_object_vars($this));
    }

    public function getRespons($data)
    {
        return ResponseAccount::setParam($data);
    }

    public function getPayCode()
    {
        return $this->payCode;
    }
}
