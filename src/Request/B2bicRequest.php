<?php

namespace B2bic\Request;

use Exception;
use B2bic\Commond\Tool;
use B2bic\Response\B2bicResponse;
use B2bic\Commond\B2bicRequestInterface;

class B2bicRequest
{
    private $url = '127.0.0.1:7072';
    private $version = 'A001';
    private $sysId = '01';
    private $charset = '02'; //01：GBK 02：UTF8 03：unicode 04：iso-8859-1
    private $methon = '02'; //01:tciip 02:http 03webservice
    private $customerId = '00901080000008888000';
    private $bodyLength = 0;
    private $payCode = '000000';
    private $operationId = '00000';
    private $type = '01';
    private $payDay = '';
    private $payTime = '';
    private $code = '';
    private $responseCode = '000000';
    private $resonseMsg = '';
    private $isEnd = '0';
    private $requestTimes = '001';
    private $signType = '0';
    private $signDataType = '1';
    private $signMethen = 'RSA-SHA1';
    private $signLenth = 0;
    private $fileSize = 0;
    private $body = '';
    private $requestStr = '';

    public function __construct()
    {
        $this->resonseMsg = str_pad('', 100);
        $this->signMethen = str_pad('RSA-SHA1', 12);
        $this->signLenth = str_pad('0', 10, '0', STR_PAD_LEFT);
    }

    // public function setSysId($sysId)
    // {
    //     $this->sysId = str_pad($sysId, 2);
    //     return $this;
    // }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function setRequestTimes($times){
        $this->times = str_pad($times , 3, '0', STR_PAD_LEFT);
        return $this;
    }

    public function setCustomerId($customerId)
    {
        $this->customerId = str_pad($customerId, 20);
        return $this;
    }

    // public function setPayCode($payCode)
    // {
    //     if(strlen($payCode) >6){
    //         $payCode = substr($payCode,0,6);
    //     }
    //     $this->payCode = str_pad($payCode, 6);
    //     return $this;
    // }

    public function setCode($code)
    {
        if (strlen($code) > 20) {
            $code = substr($code, 0, 20);
        }
        $this->code = str_pad($code, 20);
        return $this;
    }

    /**
     * 发送请求到b2bic系统
     *
     * @param B2bicRequestInterface $res
     * @param boolean $debug
     * @return B2bicResponse
     */
    public function execute(B2bicRequestInterface $res, $debug = false)
    {
        $data = $res->getParam();
        $this->payCode = str_pad($res->getPayCode(), 6);
        $this->body = Tool::arrToXml($data);
        $this->payDay = date('Ymd');
        $this->payTime = date('His');
        $this->setBodyLength()->createRequestStr();
        // var_dump($this->requestStr);
        // exit;

        $rs = Tool::curl($this->url, $this->requestStr);

        if ($debug) {
            file_put_contents('request.log', $this->requestStr);
            file_put_contents('response.log', $rs);
            echo '发送' . $this->requestStr;
            echo '接收' . $rs;
        }

        $response = new B2bicResponse($rs, $res);
        return $response;
    }

    private function setBodyLength()
    {
        $this->bodyLength = str_pad(strlen($this->body), 10, '0', STR_PAD_LEFT);
        return $this;
    }

    private function createRequestStr()
    {
        //组装报文
        $this->requestStr .= $this->version;
        $this->requestStr .= $this->sysId;
        $this->requestStr .= $this->charset;
        $this->requestStr .= $this->methon;
        $this->requestStr .= $this->customerId;
        $this->requestStr .= $this->bodyLength;
        $this->requestStr .= $this->payCode;
        $this->requestStr .= $this->operationId;
        $this->requestStr .= $this->type;
        $this->requestStr .= $this->payDay;
        $this->requestStr .= $this->payTime;
        $this->requestStr .= $this->code;
        $this->requestStr .= $this->responseCode;
        $this->requestStr .= $this->resonseMsg;
        $this->requestStr .= $this->isEnd;
        $this->requestStr .= $this->requestTimes;
        $this->requestStr .= $this->signType;
        $this->requestStr .= $this->signDataType;
        $this->requestStr .= $this->signMethen;
        $this->requestStr .= $this->signLenth;
        $this->requestStr .= $this->fileSize;
        $this->requestStr .= $this->body;
    }
}
