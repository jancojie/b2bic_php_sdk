<?php

namespace B2bic\Model;


/**
 * 台账实体类
 */

class Ledger
{
    private $SubAccount = ''; //子账户
    private $AccountDate = ''; //会计日期
    private $JournalNo = ''; //日志号
    private $SeqNo = ''; //顺序号
    private $TranTime = ''; //交易时间
    private $MainAccount = ''; //实体账号
    private $MainAccountName = ''; //实体账户名
    private $SubAccoutName = ''; //子账户别名
    private $DCFlag = ''; //借贷标识
    private $TranAmount = ''; //交易金额
    private $TxFlag = ''; //调整交易标识 Y - 余额调整 空或N - 非余额调整
    private $OppAccountNo = ''; //对方账号（实体）
    private $OppAccountName = ''; //对方账户名
    private $OppBankName = ''; //对方户名
    private $OppBankNo = ''; //对方行号
    private $Remark = ''; //备注
    private $Balance = ''; //交易后余额
    private $BizFlowNo = ''; //业务流水

    public function __construct($arr)
    {
        $this->setParam($arr);
    }

    public function getSubAccount()
    {
        return $this->SubAccount;
    }

    public function getAccountDate()
    {
        return $this->AccountDate;
    }

    public function getJournalNo()
    {
        return $this->JournalNo;
    }

    public function getSeqNo()
    {
        return $this->SeqNo;
    }

    public function getTranTime()
    {
        return $this->TranTime;
    }

    public function getMainAccount()
    {
        return $this->MainAccount;
    }

    public function getMainAccountName()
    {
        return $this->MainAccountName;
    }

    public function getSubAccoutName()
    {
        return $this->SubAccoutName;
    }

    public function getDCFlag()
    {
        return $this->DCFlag;
    }
    public function getCcyCode()
    {
        return $this->CcyCode;
    }
    public function getTranAmount()
    {
        return $this->TranAmount;
    }
    public function getTxFlag()
    {
        return $this->TxFlag;
    }

    public function getOppAccountNo()
    {
        return $this->OppAccountNo;
    }

    public function getOppAccountName()
    {
        return $this->OppAccountName;
    }

    public function getOppBankName()
    {
        return $this->OppBankName;
    }

    public function getOppBankNo()
    {
        return $this->OppBankNo;
    }

    public function getRemark()
    {
        return $this->Remark;
    }

    public function getBalance()
    {
        return $this->Balance;
    }

    public function getBizFlowNo()
    {
        return $this->BizFlowNo;
    }

    public function setParam($data)
    {
        // $obj = new self();
        if (is_array($data)) {
            $data = array_filter($data);
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        }
        // return $obj;
    }
}
