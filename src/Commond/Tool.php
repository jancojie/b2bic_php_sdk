<?php

namespace B2bic\Commond;

use Exception;
use SimpleXMLElement;

class Tool{

    public static function curl($url,$data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:text/xml;charset=utf-8'
        ));
        $content = curl_exec($ch);

        if ($content === false) {
            throw new Exception('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $content;
    }
    
    //数组转xml
    public static function arrToXml($array, $rootElement = null, $xml = null)
    {
        $_xml = $xml;
        // 如果没有$rootElement，则插入$rootElement
        if ($_xml === null) {
            $_xml = new SimpleXMLElement($rootElement !== null ? $rootElement : '<?xml version="1.0" encoding="utf-8" ?><result/>');
        }
        // 访问所有键值对 
        foreach ($array as $k => $v) {
            // 如果有嵌套数组 
            if (is_array($v)) {
                // 调用嵌套数组的函数
                self::arrToXml($v, $k, $_xml->addChild($k));
            } else {
                $_xml->addChild($k, $v);
            }
        }
        return $_xml->asXML();
    }

    //Xml转数组
    public static function xmlToArr($xml)
    {
        if ($xml == '') return '';
        libxml_disable_entity_loader(true);
        $arr = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $arr;
    }


    /**
     * 验证第六位数字是否正确
     *
     * @param  $SubAccountSeq
     * @return void
     */
    public static function checkSubAccountSeq($SubAccountSeq){
        //第一步获取左边五位数
        $code = explode('.',$SubAccountSeq/10);
        // var_dump($code);
        $code[0] = str_pad($code[0], 5, '0', STR_PAD_LEFT);

        $checkArr = str_split($code[0]);

        //奇数位集合
        $checkArr_odd = array_filter($checkArr, function ($var) {
            return ($var & 1);
        }, ARRAY_FILTER_USE_KEY);

        //偶数位集合
        $checkArr_even = array_filter($checkArr, function ($var) {
            return !(($var & 1));
        }, ARRAY_FILTER_USE_KEY);

        $c1 = 0;
        foreach($checkArr_odd as $num){
            //计算奇数位的和
            $c1 += intval($num);
        }

        $c2 = 0;
        foreach ($checkArr_even as $num) {
            //计算偶数位的和
            $c2 += intval($num);
        }

        //奇数位的和与偶数位的和的三倍相加
        $c3 = $c1 + $c2*3;
        //取个位数
        $c4 = $c3 % 10;
        //使用10减去上一步计算的值再取整
        $checkNum = intval((10 - $c4)%10);

        if($code[1] == $checkNum){
            return true;
        }else{
            return false;
        }

    }

    /**
     * 生成验证位
     *
     * @param $code
     * @return void
     */
    public static function calculateCehckNumber($code){

        $code = str_pad($code, 5, '0', STR_PAD_LEFT);

        $checkArr = str_split($code);

        //奇数位集合
        $checkArr_odd = array_filter($checkArr, function ($var) {
            return ($var & 1);
        }, ARRAY_FILTER_USE_KEY);

        //偶数位集合
        $checkArr_even = array_filter($checkArr, function ($var) {
            return !(($var & 1));
        }, ARRAY_FILTER_USE_KEY);

        $c1 = 0;
        foreach ($checkArr_odd as $num) {
            //计算奇数位的和
            $c1 += intval($num);
        }

        $c2 = 0;
        foreach ($checkArr_even as $num) {
            //计算偶数位的和
            $c2 += intval($num);
        }

        //奇数位的和与偶数位的和的三倍相加
        $c3 = $c1 + $c2 * 3;
        //取个位数
        $c4 = $c3 % 10;
        //使用10减去上一步计算的值再取整
        $checkNum = intval((10 - $c4) % 10);

        return $code.$checkNum;
    }
}