<?php
namespace B2bic\Commond;

interface B2bicRequestInterface{

    /**
     * 获取对象字段数组
     *
     * @return Array
     */
    public function getParam();

    /**
     * 获取返回包对象
     *
     * @return obj
     */
    public function getRespons($data);

    /**
     * 获取接口编码
     *
     * @return String
     */
    public function getPayCode();

}

